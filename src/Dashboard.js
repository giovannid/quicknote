import React from "react";
import Line from "./Line";
import { loadState, saveState } from "./localStorage";
import { getUserNotes, saveUserNotes } from "./api";
import { v4 } from "uuid";
import { normalize } from "normalizr";
import { arrayOfNotes } from "./schema";
import { OfflineChecker } from "./offlineMode";

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notesById: [],
      notes: {},
      loading: true,
      pathname: window.location.pathname.slice(1),
      remoteUser: false,
      protected: false
    };
    this.offline = new OfflineChecker();
  }

  saveLocalStorage = _ => {
    let prepareNotes = this.prepareNotesForSave();
    let savingState = saveState(prepareNotes);
    if (savingState) {
      console.log("local notes saved");
    } else {
      console.error(`Problem saving notes in the browser.`);
    }
  };

  loadLocalStorage = _ => {
    let localState = loadState();
    this.setState(
      _ => {
        if (localState !== undefined) {
          let normalized = normalize(localState, arrayOfNotes);
          return {
            notesById: normalized.result,
            notes: normalized.entities.notes,
            loading: false
          };
        } else {
          return {
            loading: false
          };
        }
      },
      _ => this.addBlankNote()
    );
  };

  loadRemoteServer = username => {
    let remoteState = getUserNotes(username);
    remoteState.then(res => {
      let { success, error, ...rest } = res;
      this.setState(
        _ => {
          if (success && !error) {
            let normalized = normalize(rest.notes, arrayOfNotes);
            return {
              notesById: normalized.result,
              notes: normalized.entities.notes,
              loading: false,
              remoteUser: true
            };
          } else {
            console.error("Could not load remote notes.");
            return {
              loading: false
            };
          }
        },
        _ => this.addBlankNote()
      );
    });
  };

  saveRemoteServer = username => {
    let prepareNotes = this.prepareNotesForSave();
    saveUserNotes(username, prepareNotes).then(res => {
      console.log(res);
      let { success, error } = res;
      if (success && !error) {
        console.log("remote notes saved");
      } else {
        console.error(`Could not save notes to remote host.`);
      }
    });
  };

  handleLoad = _ => {
    if (this.state.pathname !== "") {
      this.loadRemoteServer(this.state.pathname);
    } else {
      this.loadLocalStorage();
    }
  };

  handleSave = _ => {
    if (this.state.remoteUser) {
      this.saveRemoteServer(this.state.pathname);
    } else {
      this.saveLocalStorage();
    }
  };

  componentDidMount() {
    this.handleLoad();
    this.offline.watchForNetworkChange(isOnline => {
      console.log(`isOnline: ${isOnline}`);
    });
  }

  componentWillUnmount() {
    this.offline.stopWatchingForNetworkChanges();
  }

  addBlankNote = _ => {
    let newId = v4();
    this.setState(prevState => ({
      notesById: prevState.notesById.concat([newId]).reverse(),
      notes: {
        [newId]: {
          id: newId,
          order: 0,
          value: ""
        },
        ...prevState.notes
      }
    }));
  };

  saveNote = note => {
    this.setState(
      prevState => ({
        notes: { ...prevState.notes, [note.id]: note }
      }),
      _ => {
        this.handleSave();
      }
    );
  };

  removeNote = (noteid, emptyNote) => {
    this.setState(
      prevState => ({
        notesById: prevState.notesById.filter(id => id !== noteid)
      }),
      _ => {
        if (!emptyNote) this.handleSave();
      }
    );
  };

  getNote = id => {
    return this.state.notes[id];
  };

  getNotes = _ => {
    const { notesById } = this.state;
    return notesById.map(id => this.getNote(id));
  };

  prepareNotesForSave = _ => {
    let notes = this.getNotes();
    return notes.filter(note => note.value.trim() !== "");
  };

  render() {
    const { loading } = this.state;
    return (
      <div className="Dashboard">
        {loading ? (
          <p>Loading... </p>
        ) : (
          <React.Fragment>
            <div className={`Sidebar`} />
            {this.getNotes().map((note, index, array) => (
              <Line
                key={note.id}
                id={note.id}
                order={note.order}
                value={note.value}
                lastItem={0 === index}
                addBlankNote={this.addBlankNote}
                saveNote={this.saveNote}
                removeNote={this.removeNote}
              />
            ))}
          </React.Fragment>
        )}
      </div>
    );
  }
}
