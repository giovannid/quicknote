import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faPenAlt as faPen } from "@fortawesome/free-solid-svg-icons";

class Line extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
    this.state = {
      value: props.value || "",
      isFocused: false
    };
  }

  componentDidMount() {
    this.watchForPaste();
    this.watchForKeyPress();
  }

  watchForKeyPress = _ => {
    window.addEventListener("keypress", e => {
      if (!e) {
        var e = window.event;
      }
      if (this.input.current !== null) {
        if (this.state.isFocused === false) {
          if (
            (e.keyCode >= 65 && e.keyCode <= 90) ||
            (e.keyCode >= 97 && e.keyCode <= 122) ||
            (e.keyCode >= 48 && e.keyCode <= 57)
          ) {
            this.input.current.focus();
          }
        } else {
          // check if enter was pressed
          if (e.keyCode === 13) {
            this.input.current.blur();
          }
        }
      }
    });
  };

  watchForPaste = _ => {
    window.addEventListener("paste", e => {
      let paste = e.clipboardData.getData("text").trim();
      if (paste !== "" && this.input.current !== null) {
        console.log(`PASTED: `, paste);
        this.input.current.focus();
      }
    });
  };

  handleDoubleClick = _ => {
    this.setState({ isFocused: true }, _ => {
      this.input.current.focus();
    });
  };

  handleLineClick = e => {
    const inp = document.createElement("input");
    inp.value = e.target.textContent;
    document.body.appendChild(inp);
    inp.select();
    document.execCommand("copy", false);
    inp.remove();
  };

  handleInputChange = e => {
    this.setState({ value: e.target.value });
  };

  handleBlur = _ => {
    const { id, order, value: propsValue } = this.props;
    const { value } = this.state;
    this.setState({ isFocused: false }, _ => {
      if (propsValue !== value) {
        if (this.props.lastItem) {
          this.props.addBlankNote();
        }

        this.props.saveNote({
          id,
          order,
          value
        });
      }
    });
  };

  handleFocus = e => {
    this.setState({ isFocused: true });
  };

  handleRemoveClick = _ => {
    if (!this.props.lastItem) {
      let emptyNote = this.state.value.trim() === "" ? true : false;
      this.props.removeNote(this.props.id, emptyNote);
    }
  };

  handleEditClick = _ => {
    if (this.props.value !== "") this.handleDoubleClick();
  };

  render() {
    const { value, isFocused } = this.state;
    const { lastItem } = this.props;
    return (
      <div className="Line">
        {value === "" ||
        this.input.current === document.activeElement ||
        isFocused ? (
          <input
            tabIndex={1}
            type="text"
            className="TextField"
            placeholder="Type something..."
            value={value}
            ref={this.input}
            onChange={this.handleInputChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
          />
        ) : (
          <p
            onClick={this.handleLineClick}
            onDoubleClick={this.handleDoubleClick}
            className="TextPresenter"
          >
            {value}
          </p>
        )}
        <button
          tabIndex={-1}
          onClick={this.handleEditClick}
          disabled={value !== "" && !isFocused ? false : true}
          className={`EditButton ControlButtons`}
        >
          <FontAwesomeIcon icon={faPen} size="lg" />
        </button>
        <button
          tabIndex={-1}
          onClick={this.handleRemoveClick}
          disabled={lastItem || isFocused ? true : false}
          className={`RemoveButton ControlButtons`}
        >
          <FontAwesomeIcon icon={faTimes} size="lg" />
        </button>
      </div>
    );
  }
}

export default Line;
