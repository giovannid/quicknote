const endpoint = "https://9o0l47ly8y.sse.codesandbox.io/v1";

export const getUserNotes = username =>
    makeApiRequest({
        url: username,
    });

export const saveUserNotes = (username, notes) =>
    makeApiRequest({
        url: username,
        data: notes,
        method: "POST",
    });

const makeApiRequest = args => {
    let { url, data, method = "GET" } = args;
    return new Promise(resolve => {
        fetch(`${endpoint}/${url}`, {
            method,
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        })
            .then(res => res.json())
            .then(res => resolve(res))
            .catch(err => resolve(err));
    });
};
