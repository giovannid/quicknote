import React from "react";
import ReactDOM from "react-dom";
import "./css/style.scss";

import Dashboard from "./Dashboard";

ReactDOM.render(<Dashboard />, document.getElementById("root"));
