//TODO encrypt local storage data
export const loadState = _ => {
    try {
        const serializedState = localStorage.getItem("notes");
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem("notes", serializedState);
        return true;
    } catch (err) {
        console.error(err);
        return false;
    }
};
