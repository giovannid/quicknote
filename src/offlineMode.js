export class OfflineChecker {
  constructor() {
    this.isOnline = navigator.onLine || false;
    this.callback = null;
  }

  addListeners() {
    window.addEventListener("online", this.updateOnlineStatus);
    window.addEventListener("offline", this.updateOnlineStatus);
  }

  updateOnlineStatus = _ => {
    let self = this;
    self.isOnline = navigator.onLine ? true : false;
    self.callback(self.isOnline);
  };

  stopWatchingForNetworkChanges() {
    window.removeEventListener("online", this.updateOnlineStatus);
    window.removeEventListener("offline", this.updateOnlineStatus);
  }

  watchForNetworkChange(callback) {
    this.addListeners();
    this.callback = callback;
    this.updateOnlineStatus();
  }
}
