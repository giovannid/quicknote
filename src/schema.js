import { schema } from "normalizr";

export const note = new schema.Entity("notes");
export const arrayOfNotes = new schema.Array(note);
